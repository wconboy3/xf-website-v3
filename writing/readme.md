## Welcome

This is the writing section. Contributers to the Xalgorithms Foundation can share writing here that is related to their efforts within working groups, or their efforts outside of working groups that utilize Xalgorithms Foundation technology, or concepts.

## Editing

if you want to make edits on a blog post or contribute a new piece of writing run `$ git checkout drafts`, or navigate to the drafts branch. 

If you open the file in the Web IDE, the text will wrap. Or open localy in your text editor of choice.

Use the following method to write inline comments:

``[//]: # (You can write comments like this, and they will be visible to editors, but will not be rendered on the web page)``

Alternatively you can open an issue realting to an edit. Be sure to tag the author you'd like to look over the changes.

You can link to a specific line in the document by opening the web editor, hovering to the left of the line number, right clicking the link incon, and selecting copy url. You may then utilize that address in an issue you may want to create.
