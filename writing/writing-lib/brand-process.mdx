---
title: 'Brand Process'
date: 'Jan 11'
featimg: './brand-process-hero.jpg'
featimgalt: ''
summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit'
author: 'Calvin Hutcheon'
authorProfile: ''
authorDescription: ''
tags: [
    {tag: 'brand' },
    {tag: 'another tag'}
]
version: '1.0'
published: 'true'
---
Throughout its history, the Xalgorithms Foundation has undergone multiple attempts at branding. As of writing this, different logomarks are used across platforms and documents. It's clear that some sort of strategy needs to be articulated. In short, does a brand make sense for our decentralized community, and if so, what does it look like? 

This document attempts to provide answers to these questions. It lays out the creative processed used to arrive at these answers, and proposes a logomark.
#### Background
The Xalgorithms Foundation has had three different logos. Each of these managed to capture aspects of the organization. Each of these also has issues that make it challenging for use within a decentralized organization.

Logo | Challenges
-- | --
![](/xf_logo_v1.png) | Logo Mark adds little of significance. The visual language does not extend into material components, or other elements used to build a brand across applications.
![](/xf_logo_v2.png) | The logo is detached from the mission. Similarly, the visual language does not extend into material components, or other elements used to build a brand across applications.
![](/xf_logo_v3.png) | Derived from the RM dev interface, this logo highlights the characteristics of tabular programming and trinary logic. However, it unintentionally blends elements of the software and system design in a way that causes confusion. 

Given all of these attempts, why didn't anything stick? At its heart, branding the Xalgorithms Foundation is a challenging task. There are a number of requirements that take significant thought to accomplish. For example, the brand must be simple enough that it can be widely shared and executed. At the same time, it must speak to concepts that are abstract and complex. Finally, it must be flexible enough to host multiple sub-brands.
#### Why a Brand?
With these overlapping challenges, does it make sense to have a brand? In short yes. In fact having a coherent brand strategy may be even more important for a decentralized, community driven protocol, than for more traditional forms of organization. In a [Headless Brand](https://otherinter.net/web3/headless-brands/) the authors write: <span className="sideQuote">Shorin et al, also helped to clarify the need for a visual distinction between the protocol and software used to interact with it.</span>  "Brand is one of the strongest assets for decentralized protocols. A headless brand strategy is an ecosystemic affair and entails the mobilization of a decentralized set of actors. At its core, it revolves around giving agency to different stakeholders in a way that lets them coordinate more effectively and feel connected to the brand." Our challenge is then how to create an open brand that allows room for contributors to fill in the rest. 
#### Grounding

This is a daunting task. Fortunately there are guiding design principals from the forthcoming Oughtomation paper that can provide a starting point for the development of a brand. <span className="sideQuote">All the sources quoted below are surfaced in the fourthcoming Oughtomation Paper</span> The following 4 design virtues became key points of orientation.

**Simplicity**

Saint-Exupéry praises the way 

_"the human_ <span className="sideQuote">Saint-Exupéry, 2010, pp. 41–42</span> _eye will follow with effortless delight [..] a line that had not been invented but simply discovered, had in the beginning been hidden by nature and in the end had been found by the engineer."_ 

A similarly intuitive form is sought here.

**Tolerance** 

Joseph Potvin describes the concept as follows: 

_"Tolerance encompasses a designer’s spirit of respect for the prerogatives of those who are users of a design, or who are subject to its result, but also those across the community of other designers who would engage with the designed work in their preferred ways, for their own purposes, within their chosen domains, using their preferred technologies, and interpreted in the contexts of their respective normative paradigms."_ 

This has interesting implications for a brand. Normally rigidly controlled, the principle of tolerance opens the possibility for a different relationship to the brand that is driven by community. 

**Least Power** 

This principal is most often employed in the context of programming languages.

_“Strength_ <span className="sideQuote">Qureshi, 2017</span>_is a weakness when it comes to programming languages. The stronger and more expressive a programming language is, the more complex its code becomes. [...] Complex programs are more difficult to reason about and harder to identify edge cases for. [...] The less the language lets you do, the easier it is to analyze and prove properties."_ 

However this can also be applied to visual language. <span className="sideQuote">Zhuge 2010, p. 202</span> "The less information a semantic path contains, the easier people understand and remember." 

#### From Brand as Archive to Brand as Palimpsest

To understand how to translate the design virtues into a graphical concept, I first looked outside the Foundation for inspiration. One source that quickly came to mind was the I Ching.

![](https://benebellwen.files.wordpress.com/2017/03/i-ching-64-hexagram-reference-table.jpg?w=739&h=436 "list of the iching characters")

 I was drawn to the fact that each character possesses the characteristic of  an archive: pointing to textual meaning, while part of a larger system, connected by an internal logic. Importantly each person enters the I Ching in their own way, finding an entry by chance or divination. 
 
 <span className="sideQuote">Consulting the I Ching for this project, I drew ䷓ <a href="https://www.eclecticenergies.com/iching/consultation?lns=888877" target="_blank">Examening</a>:
"Examining. Washed, but not offered. There is confidence in discretion [...] examining what is there, without doing anything. This cautious and unrevealing attitude makes confidence possible."
</span>

 A logo for the Xalgorithms foundation also must hold information in an archival manner, while remaining part of a cohesive system of working group identities. Similarly, chance plays a large role in how a person first begins encounter the Xalgorithms Foundation. Their background, interests, and entry point will all shape how they perceive the system design. This is made explicit in the Foundation's writing guidelines which, advocating for a writing style grounded in learning and personal discovery. The personal journey into conceptual work has proved to be the most effective way to communicate. 

As I was thinking about the I Ching, I was also drawn to the idea of the the palimpsest. A palimpsest is a piece of velum that had been used by multiple scribes, the original text scraped away to make way for new words. However, this process is imperfect, leaving behind traces of the original text. 

Given the previous brand iterations, and the fact that the Xalgorithms Foundation engages in a "design as research methodology" it seems fitting to bring this conceptual element into the brand design. The old work is never gone. It bleeds through in places.

![](https://upload.wikimedia.org/wikipedia/commons/c/c5/Codex_Nitriensis%2C_f.20r_%28Luke_9%2C22-33%29.jpg "ancient text with multiple passages scribe over top one another")

#### In Combination

With these ideas in mind, I began to explore possible direction. I wanted to stay close to the previous logo that represented concepts of trinary logic and tabular programming. The question became how to represent those ideas without the mark becoming derivative of the visual language of the XRM dev interface. I knew I needed to abstract the ideas represented. However, the previous mark was already quite abstract, using simple shapes and color. The path forward was not obvious.

The answer began to come into focus looking back at previous design work undertaken for the Xalgorithms foundation. The previous website iteration, and the prototypical lichen interface gave me the inspiration I needed. <span className="sideQuote">Lichen is a UI designed to highlight multiple pieces of data without imposing hierarchy of an unordered list.</span> The use of circles, and the organic forms gave me inspiration for 

Taking the idea of quantrany logic and abstracting it though the use of circles, I not only retained the core mechanics central to the Foundation, I was also able to allow layers of Xalgorithms History permeate the brand.
![](/lichen.png)

<Caption>
The lichen UI.
</Caption>
  
At the same time, this idea was abstract enough that it could speak to multiple elements of the Foundation. The circles refer to quatrinary logic. They can also speak to the idea that the Foundation is comprised of many working groups. Or, the circles could be rule reserves: each a network of their own.

However, this treatment seemed a little static. The concept was there but the form was not quite complete. The final touch was to realize that this organization is a system, not just that it is a complex system. Systems theory tells us that it is difficult to see a complex system without perturbation. So to make the system visible, I added some perturbation.

<Logo />

<Caption>
Use the mouse to interact with the logo to see how it could function as a complex system.
</Caption>

Taking a still from this results in the identity for the foundation.


#### In Conclusion 

This is a simple yet flexible identity. It holds history of the organization, and embodies the core design virtues. To conclude I'll touch on how the brand touches on the core design virtues. 

<RandomLogo />

**Simplicity** 

The forms of the logo were discovered. It was by looking back at the history of the organization, and thinking about the logo as a form that can hold history that the conceptual meaning emerged. And it was only through simulating the particles that the formal mark took shape. In both case, the form was found.

**Tolerance**

At the same time, as the designer, I claim no monopoly over the identity. The fact that I did not produce a lock up is intentional. The community is encourage to reproduce the mark on their own. Given the simplicity of logo mark, it can be assembled with simple instructions.

Moreover, this can be completed in many mediums. It can be constructed in a digital paint tool. It can be sketched by hand. Or, it can be rendered in ASCII characters

```jsx
// a constelation among the stars
*                           *         .
    ●  •  ●        *          
    •  ●  .          . 
    ●  .  •                                     . 
.                           *
```

Or hand drawn -> featured hand drawn version

**Least Power** 

The logo can be reconfigured, to represent working groups within the foundation. changing the sizes of the circles and the way in which the grid is broken, new working groups can be distinguished. 

Moreover The logo can be described using simple rules. So to conclude this write up, I want to express the logo mark as a set of instructions. 

- compose a 4x4 grid

- draw a circle for every intersecting line in the grid so that the point of intersection is contained somewhere within that circle.

- draw the circles with a variety of diameters, with the larges being equal to the measurement of a grid square, and the smallest being.

- To create the foundation's logo, there will be 4 (0-3) sized circles used. The size will be laid out on the grid as follows


 . | a | b | c 
 -- | -- | -- | --
 A | 3 | 2 |3 
 B | 2| 3 | 0
 C | 3 | 1 | 2