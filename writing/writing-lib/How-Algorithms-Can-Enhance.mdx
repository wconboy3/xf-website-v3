---
title: 'How Algorithms Can Enhance ‘Trade Tech’: On Rules, Borders and the Internet'
date: 'Mar 15'
featimg: 'writing-images/halifax-port.jpg'
featimgalt: ''
summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit'
author: 'Craig Atkinson'
authorProfile: '/profiles/i-craig.JPG'
authorDescription: 'Since 2016, Craig has cultivated a novel model of global trade regulation - ‘Trade Policy 3.0’, enabled by an ‘Internet of Rules’ - arising through Xalgorithms’ applied R&D.'
tags: [
    {tag: 'trade' },
    {tag: 'another tag'}
]
version: '1.0'
published: 'true'
---

<br/>

#### On a Mission: Minimizing the Cost and Friction of International Trade

Growing up in Canada’s East Coast port city of Halifax, it was always interesting to wonder what was arriving at our shores via the massive container ships that would appear in the harbour. With one of the deepest harbours in the world (Australia’s Sydney Harbour is the deepest), Halifax is able to accommodate super-post-Panamax vessels. Yet, like any ‘trade city’, structural conditions largely dictate what is (or isn’t) arriving through the local terminals.

Lately, I’ve been focused on thinking about ways to improve the utilization of tangible and intangible resources so that buyers and sellers conduct transactions with an optimum number of intermediaries and a minimum amount of cost/friction. After working in trade development for the United Nations, two national governments and in the private sector, this interest led me to Xalgorithms Foundation and its alliance of contributors.

Through a novel approach to address how we express and communicate normative rules via Internet technology, the Xalgorithms Alliance has created a model of collaboration, methods and software that could enhance the development and use of ‘trade tech’ (what the World Economic Forum describes as, "the set of technologies and innovations that enable trade to be more efficient, inclusive, and equitable)". <SideQuote><a href="https://www.weforum.org/reports/mapping-tradetech-trade-in-the-fourth-industrial-revolution">Mapping TradeTech: Trade in the Fourth Industrial Revolution</a></SideQuote>  

#### From the Old World to the New

The ancient ‘law of merchants’ – the lex mercatoria – provided commercial rules along trade routes in Europe during the middle ages. These customary principles and best practices offered a normative architecture for how merchants ought to conduct business.

As goods moved from source to destination markets across the continent, the lex mercatoria fostered clarity amongst different locales and commercial traditions. Before the development of an international system of countries, these ‘rules without a state’ helped lead a resurgence in intra-European trade that had not been seen since Roman times. 

Today, a convoluted patchwork of ‘rule sets’ may be relevant to any commercial transaction. In addition to private obligations between buyers, sellers and intermediaries (a ‘market’), public international laws (rules, norms and standards between countries) create a layer of compliance requirements in cross-border contexts. Customs agencies have replaced the medieval ‘toll booth’ and, in conjunction with other government bodies, fulfil a mandate that goes far beyond taxation to include public safety and national security.

Technological change and the beginnings of a ‘fourth industrial revolution’ have also expanded the ways in which economic activity may occur. A ‘buy, ship, pay’ model of data interactions has emerged in international supply chains and the read/write Internet is gradually replacing paper-based processes to facilitate trade in goods. <SideQuote><a href="https://unece.org/fileadmin/DAM/cefact/brs/BuyShipPay_BRS_v1.0.pdf">The Buy-Ship-Pay Reference Data Model (BSP-RDM) developed by UN/CEFACT.</a></SideQuote>

Over the past 25 years, the Internet has incrementally supplanted physical media as the primary medium for sales, supply and payment. But there has been a lingering problem to solve: timely access to applicable rules and trusted reference data sources that have become the core drivers and enablers of international commercial transactions. 

![](/writing-images/value-chain.jpg)

<Caption>
Graphic Addapted from <a href="https://www.dfat.gov.au/sites/default/files/australia-singapore-digital-trade-standards-presentation.pdf" target="_blank">Australia Singapore Digital Trade Standards Presentation</a>
</Caption>

#### More Rules… More Problems

Although commercial rules are intended to provide coherence, it's up to individuals and businesses to discover rules that are in effect (often hidden in a variety of documents and maintained by various ‘rule makers’) and which rules apply (typically written in ‘legalese’ through diverse natural languages).  

[![](/writing-images/nathan-for-you.png)](https://www.facebook.com/ComedyCentral/videos/10155720977844030/)

<Caption>
Comedian Nathan Fielder parodies Customs brokerage and determination of which tariff rule may apply - he attempts to reclassify smoke detectors as a musical instrument to achieve a lower tariff rate.
</Caption>

When all requirements are identified, the compliance step involves operationalizing rules through the submission of documentation (an assembly of data elements) that represents an administrative barrier and an imperfect proxy for the rules. While negotiators spend years to agree on the preferential treatment of trade between countries, issues associated with rule discovery and compliance make participation in trade challenging for small businesses and the situation is exacerbated by several conditions. 

For one, the proliferation of trade agreements between countries has led to a  ‘spaghetti bowl’ of policies: there are more and more rules at both international and domestic levels. 

Also, rules may be more challenging to comply with given the rise of ‘global value chains’. For example, Customs ‘Rules of Origin’ (RoOs) take into account where and how much value is added as a product moves from conception to consumption to determine origin. It may not be that these rules are too vague in their formula, but it may be challenging to provide data as ‘proof’ of origin. Additionally, customs valuations may take into consideration intangible value, such software that was conceptualized and developed in another country and is installed on a device. 

Compounding these issues, ‘second generation’ trade agreements have begun to address the digital economy and cross-border transfers of data. Well intentioned, paper-based rules may have reached their functional limits when it comes to regulating the flow of goods, services and data across borders.

#### Enhancing ‘Digital’ Infrastructure’ for Trade:  My Perspective and Xalgorithms

I’m motivated by attempting to solve problems associated with rule systems. In 2016, I was introduced to the Xalgorithms Alliance of contributors developing free, libre and open source software to enhance the flow of normative data across the Internet.

A novel approach at the core of these efforts, the ‘Oughtomation’ method allows for the expression of rules in the form of computable data based on a simple grammatical structure that can be understood by humans and computers alike. In this form, known as Xalgo Rule Schema (‘XalgoRS’), the logic of rules can be fulfilled by relevant criteria or data. Hence, rules expressed using the ‘Oughtomation’ method can be considered ‘Rules as Data’.

To obtain information on normative rules and operationalize their use, ‘Rules as Data’ can be published to the Internet, discovered, fetched, evaluated and automated. In conjunction with distributed systems like the InterPlanetary File System (IPFS), the online publication of computational rules in XalgoRS leads to a networked repository of rules. This extends the Internet toward a declarative ‘is/ought’ mesh, and what Xalgorithms has referred to as an ‘Internet of Rules’ (IoR), intended as a general-purpose enhancement.

My interest was piqued by the Xalgorithms model because I could see how commerce-related use cases for an IoR could apply in many contexts, including government to business (G2B), government to government (G2G) and business to business (B2B) interactions. Auxiliary to the written word of public and private rules systems, an IoR creates a mechanism for determining trade compliance requirements, performing calculations and automating processes.

I’ll try to explain concisely how I perceive this could enhance existing and envisaged ‘trade tech’ and streamline international commercial transactions.

#### One Internet, Many ‘Single Window Systems’

An ‘Internet of Rules’ answers the question: “How do the rules get into a system?”

According to the United Nations Center for Trade Facilitation and Electronic Business (UN/CEFACT) recommendations, a ‘single window’ system, "allows parties involved in trade and transport to lodge standardized information and documents with a single entry point to fulfill all import, export, and transit-related regulatory requirements… if information is electronic, then individual data elements should only be submitted once." <SideQuote><a href="https://unece.org/fileadmin/DAM/cefact/recommendations/rec33/rec33_trd352e.pdf" target="_blank">“Recommendation and Guidelines on Establishing a Single Window to Enhance the Efficient Exchange of Information Between Trade and Government"</a></SideQuote>

![](/writing-images/network-of-networks.jpg)

<Caption>
Example of a single window <a href="http://tfig.itcilo.org/cases/Mozambique.pdf" target="_blank">“network of networks”</a> of public and private actors.
</Caption>

Importantly, a standard way to publish computational rules to the Internet can support the development of national single window systems for Customs (and their interface with each other) as well as banking and transportation / logistics networks. Where rules have been expressed and published in a simple computational form like XalgoRS, an IoR can help lower or eliminate market barriers (e.g. capability requirements and/or excessive transaction costs).

Thus, a use case for an IoR is to enable key functions in support of trade facilitation and cross-border e-commerce in a way that does not confine them to any given silo. This makes trade more ‘fair’ for businesses of different sizes.

#### Rebranch to Update the ‘Rules of the Trade Game’

Whether for trade or other use cases, the general-purpose specifications and reference implementations of the three components (RuleMaker, RuleFinder and RuleTaker) designed by Xalgorithms contributors are complementary and supportive of various computational law and ‘Rules as Code’ initiatives. With years of philosophical and practical consideration, the overall approach is:

- **Human-centered**: Making it easier for people to access, understand, express or use rules, every aspect of ‘Xalgorithms thinking’ is human-centric. As a general purpose method for improved natural language (any) drafting of rules and expression, ‘Oughtomation’ fulfills two key criteria: rules can be simply authored and understood in their computational form. No ‘code’, just ‘Rules as Data’. 

- **Modular**: Every component developed by Xalgorithms contributors stands on its own and can be adopted in whole, in part or rebuilt from the ground up.

- **Isomorphic**: From the Greek “iso” (equal) and “morphism” (shape or form), the concept of isomorphism can refer to whether or not a rule expressed as an algorithm is true to the logic of its natural language form. While the “letter of the law” exists as text in a trade agreement or cross-border contract, the rules of trade are delivered through proxies of various forms. Rules expressed using the ‘Oughtomation’ method are relatively isomorphic in comparison to proxy documents and can be executed by input data from forms/documents.

- **Functionally Equivalent**: Referenced from the United Nations Commission on International Trade Law (UNCITRAL), the "functional equivalent approach" is based on an analysis of the purposes and functions of the traditional paper-based requirement with a view to determining how those purposes or functions could be fulfilled through electronic means.

- **Interoperable**: Rules in Xalgo form are compatible for the purpose of exchange and can be used with various systems. Along with semantic standards, an internet of digital versions of rules to enable may create interoperability across Customs systems and power the growing network of private platforms, bridging the gap between Enterprise Resource Management (ERP), marketplaces and regulatory bodies.

- **NOT Artificial Intelligence (AI)**: ‘Rules as Data’ isn’t artificial intelligence (which implies inference). The Xalgorithms approach is deterministic and enables simple, fast rudimentary symbol-matching and filtering. Outputs of using the ‘Oughtomation’ method are auxiliary to written rules and only enhance accessibility and automation potential. Rules in these simple algorithmic forms do not make decisions or supersede the authority of parties involved in trade or commerce. They do, however, advance their access to rules that are ‘in effect’ and ‘applicable’, and this helps stakeholders to interact knowledgeably and efficiently with such rules. 

- **Tested / Modelled**: Not only are Xalgorithms methods and software subject to testing and iterative development, but one of the core reference implementations is in the form of an agent-based model (ABM) on the NetLogo platform. The purpose is to help researchers consider how rule changes may affect the environment, economy and society.




