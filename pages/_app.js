import 'xf-material-components/package/config/rootStyle.css'
import '../styles/global.css'
import NavBar from '../components/navBar'
import { useState, createContext, useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'
import tagSelect from '../lib/tagSelect'
import Footer from '../components/Footer'
import Tag from '../components/Tag';

export default function App({ Component, pageProps }) {

  const TagContext = createContext(null);

  const [lang, setLang] = useState("english");
  const [tag, setTag] = useState("all")

  

  function handleLanguageChange(e){
    setLang(e.target.value);
  };

  function filterTag(e) {
    setTag(e)
  }
  

  return (
  <div className="hold">
      <NavBar
        select={handleLanguageChange}
        setLang={lang}
        options={[
          { value: 'english', label: 'English' },
          /*{ value: 'spanish', label: 'Spanish' },
          { value: 'france', label: 'French' },*/
        ]}
      />
      <div id="mainhold">
        
        <LanguageSelect.Provider value={lang}>
          <tagSelect.Provider value={{tag, filterTag}}>
            <Component {...pageProps} />
          </tagSelect.Provider>
        </LanguageSelect.Provider>
      </div>
      <Footer />
  </div>
  );
}
