import Head from 'next/head'

export default function donate() {
    return (
      <div className="container">
        <Head>
          <title>Xalgorithms Foundation</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main>
            <h1>
                Donate
            </h1>
        </main>
      </div>
    )
  }