import Head from 'next/head'
import AboutAuthor from '../components/AboutAuthor'
import communityContent from '../content/communityContent'

import { useContext } from 'react';
import LanguageSelect from '../lib/languageSelect'

export default function community() {
  const lang = useContext(LanguageSelect)
  const content = communityContent[lang]

  const topGrid = {
    gridArea : "1 / 2 / 1 / 6"
  }

  const sideGrid = {
    gridArea : "1 / 8 / 1 / 11",
    alignSelf: "center"
  }

  const img = {
    width: '100%'
  }

  const grida = {
    gridArea : "1 / 2 / 1 / 4"
  }

  const gridb = {
    gridArea : "1 / 5 / 1 / 6"
  }

  const gridc = {
    gridArea : "1 / 8 / 1 / 9"
  }

  const linkmargin = {
    paddingTop: "1em"
  }

  const renderTeam = () => {
    return content.team.map(({img, name, bio, links}, index ) => (
      <div className="communityProfileHold" key={index}>
        <AboutAuthor image={img} name={name} description={bio} links={links}/>
      </div>
    ));
  }

  const renderWorkingGroups = () => {
    return content.workingGroups.map(({label, target}, index) => (
      <div style={linkmargin}>
        <a className="noUnderline" href={target} key={index}>
            {label} →
        </a>
      </div>
    ))
  }

    return (
      <div className="container">
        <Head>
          <title>Xalgorithms Foundation</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main>
        <div className="hero">
          <div className="WritingGrid12">
            <div className="animationTwo" className="seven-eight" >
              <h2>{content.headline}</h2>
            </div>
          </div>
        </div>
          <div className="grid12">
            <div className="communityTopGrid">
              <img src="distribute.gif" style={img}/>
            </div>
            <div className="communitySideGrid">
              <p>
                {content.intro}
              </p>
            </div>
            
          </div>
          <div className="WritingGrid12">
            <div className="sideCTA" >
              <h5>Working Groups</h5>
                {renderWorkingGroups()}
            </div>
            <div className="seven-eight" >
              <h2>People</h2>
              <div className="communityFlex">       
                {renderTeam()}
              </div>
            </div>
          </div>
        </main>
      </div>
    )
  }