// import { redirect } from 'next/dist/next-server/server/api-utils'
import Link from 'next/link'
import { getSortedPostsData } from '../lib/posts'
import sortTags from '../lib/sortTags'
import Tag from '../components/Tag'
import SignUp from '../components/SignUp'
import { useEffect, useState, useContext } from 'react'
import tagSelect from '../lib/tagSelect'


export async function getStaticProps() {

  

  const allPostsData = getSortedPostsData()
    return {
      props: {
        allPostsData
      }
    }
  }

export default function writing ({ allPostsData }) {
  const tagFlex = {
    display: 'flex',
    justifyContent: 'space-between'
  }

  const small = {
    border: 'none',
    background: 'none',
    color: 'var(--primary)',
    fontSize: '0.65em',
    textTransform: 'uppercase',
    cursor: 'pointer'
  }

  const littleMargin = {
    marginBottom: '2em',
    textTransform: 'capitalize',
    display: 'flex',
    justifyContent: 'space-between'
  }

  const bottom = {
    marginLeft: '1em'
  }


  const[tagArray, setTagArray] = useState([]);

  const renderPost = () => {
    
    return(
      (tag === "all") ? (
      <>
        <div style={littleMargin}>
                    <h2>{tag} Writing</h2>
        </div>
        <ul className="writingHold">
          {allPostsData.map(({ id, title, published, tags=[] }) => (
          (published === "true") ? ( 
            <li key={id}>
                {tags.map(({ tag }, index) => (
                  <Tag target={tag} key={index} />
                ))}
                <p className="featuredtitle">
                  {title}
                </p>
                <Link href={'/writing/'+id}>
                    <a className="noUnderline">
                      Read   →
                    </a>
                </Link>
            </li>
          ) : ( 
            null
          )
        ))}
      </ul>
    </>
    ) : (
      <>
        <div style={littleMargin}>
            <h2>{tag} Writing</h2>
            <button
                    style={small}
                    onClick={resetFilter}
                  >
                  Clear Filter
                </button>
        </div>
        <ul className="writingHold">
        {allPostsData.map(({ id, title, published, tags=[] }) => (
          (tags[0].tag === tag || tags[1].tag === tag) ? ( 
            published ? (
            <li key={id}>
                {tags.map(({ tag }, index) => (
                  <Tag target={tag} key={index} />
                ))}
                <p className="featuredtitle">
                  {title}
                </p>
                <Link href={'/writing/'+id}>
                    <a className="noUnderline">
                      Read   →
                    </a>
                </Link>
            </li>
            ) : (
              null
            )
          ) : ( 
            null
          )
        ))}
        </ul>
      </>
    )
    )
  }

  const getAllTags = () => {
    allPostsData.map(({published, tags=[]}) => {
      published ? (
        tags.map(({tag}) => {
          setTagArray(tagArray => tagArray.concat(tag))
        })
       ) : (
        null
       )
    })
  }
  
  
  const renderTags = () => {
    const sortedTags = [...new Set(tagArray)]
    return(
      sortedTags.map((index ) => (
        <Tag target={index} key={index} />
      ))
    )
  }

  
  useEffect(() => {
    getAllTags()
  }, [])

  const { tag, filterTag } = useContext(
    tagSelect
  );

  const resetFilter = () => {
    filterTag("all")
  }
  

    return (
     
        <main>
          <section>
            <div className="WritingGrid12" id="bottomMargin">
              <div className="sideCTA" >
                <h5>All Tags</h5>
                <br />
                <div className="tagsHold">
                  {renderTags()}
                </div>
              </div>
              <div className="animationTwo" className="seven-eight" >
                {renderPost()}
                <div style={bottom}>
                </div>
              </div>
            </div>
          </section>
        </main>
      
    )
  }