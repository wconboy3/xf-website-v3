 const usesContent = {
    "english": {
        uses: [
          {
            id: 'set', 
            title: 'Categorical', 
            content: 'Determine a rule in effect for an entity, product, service, status across jurisdictions. Discover a requirement, applicability, selection, or exception to trade policy, fiscal policy, etc. in real time based on web-based interaction.',
            links: [
              {target: 'test', label: 'Sample Link'},
              {target: 'test', label: 'Sample Link'}
            ]
          },
          {id: 'price', title: 'Price', content: 'Enable efficient pricing and market choice based on microeconomic and macroeconomic contexts gathered through a web-based event or interaction. Structure benchmark indices based on remote sensing data, market capitalization, and any prefered market data selection in real time.' },
          {id: 'role', title: 'Role', content: 'Attach licenses to distributed data while maintaining prerogative. Filter where data is stored and where it is not stored. Determine which queries can view such data and those which may not. Express what transformations may or may not be run with the data over any particular network or in any application.' },
          {id: 'machine', title: 'Machine', content: 'Facilitate consistent implementation of practical and ethical rules in operational mechatronics, electronics, robotics, and networking across all equipment suppliers according to jurisdictional prerogative, contractual agreement, or voluntary standards adoption.' }
        ]
    }
}

export default usesContent