const communityContent = {
    english: {
        headline: 'Community Headline',
        subhead: 'sentence about getting involved',
        primaryCTA: {
            CTA: 'Test',
            target: ''
        },
        intro: 'Welcome to the Xalgorithms Foundation community! We are a collective of collaborators around the world working together to address global concerns involving economics and its relation to technology, logic, and legal philosophy using free/libre/open source software. We offer free open source tools for citizens and authorities to communicate with one another, the community hopes to lay the foundation for a society where rule-takers and rule-makers may coexist with autonomy. Diverse areas of expertise and perspective are fundamental to our mission of designing a means of communicating rules using an interface that allows humans to interact using various spoken and written languages. Come, join us on our journey to create “an Internet of Rules”.',
        workingGroups: [
            {
                label: 'Test',
                target: ''
            },
            {
                label: 'Hello World',
                target: ''
            }
        ],
        team: [
            {
                img: "/profiles/i-joseph.png",
                name: "Joseph Potvin",
                bio: "As co-founding Executive Director of Xalgorithms, Joseph is responsible for oughtomation system design, theoretical research and use case integration. His 30-year career in applied economics and free/libre/open informatics involved inventive business analysis and design for companies, governments, multilaterals and foundations."
            },
            {
                img: "/profiles/i-don.png",
                name: "Don Kelly",
                bio: "Don Kelly is a full-stack systems designer and developer who leads oughtomation technical specifications and implemenation, and coaches the developer community. Don is a creative veteran programmer with experience in diverse projects at all layers including network, kernel, mobile, microservices, containers, data-oriented programming, and domain-specific languages."
            },
            {
                img: "/profiles/i-bill.png",
                name: "William Olders",
                bio: "Drawing upon 30 years as the founding President and CTO of a firm that specializes in high-volume rule-based transaction processing for several of the world's largest banks, credit card companies and insurance firms, Bill serves as co-founding Chair of the Xalgorithms Foundation, providing technical guidance on tabular declarative programming methods."
            },
            {
                img: "/profiles/i-ryan.png",
                name: "Ryan Fleck",
                bio: "Ryan has collaborated on Xalgorithms core technical development and testing since attending a 2018 event co-hosted by Xalgorithms and the Free Software Foundation. He had carried out end-to-end tests, and made an instructional video stepping through the functional sequence line-by-line to show others exactly how it works."
            },
            {
                img: "/profiles/i-craig.jpg",
                name: "Craig Atkinson",
                bio: "Since 2016, Craig has cultivated a novel model of global trade regulation - 'Trade Policy 3.0', enabled by an 'Internet of Rules' - arising through Xalgorithms' applied R&D. He has outlined its game-changing significance for data and trade governance in a series of articles in prominent publications, including for the World Economic Forum. "
            },
            {
                img: "/DejaHeadshot.jpg",
                name: "Deja Newton",
                bio: "Deja is an Environmental Sciences student at the University of San Francisco collaborating on Geographic Information System (GIS) data implementation in the ERA design."
            },
            {
                img: "/profiles/i-calvin.png",
                name: "Calvin Hutcheon",
                bio: "Calvin has contributed to the Xalgorithms projects conceptually and graphically since mid-2019, integrating the core system and the suite of use cases into a portfolio identify system. Calvin is attracted to unique design challenges inherent in communicating complex themes in ways that reach people with simplified depth of meaning.",
                links: [
                    { name: "website", target: "www.calvin.ooo"},
                    { name: "twitter", target: "https://twitter.com/millennialglyph"}
                ]
            },
            {
                img: "/profiles/stephane.png",
                name: "Stéphane Gagnon",
                bio: "Stéphane Gagnon is Associate Professor in Business Technology Management (BTM) at the Université du Québec en Outaouais (UQO). From the inception of Xalgorithms Foundation he has provided formal academic guidance for research relating to theoretical concepts, comuputational methods, data standards, and practical use cases."
            },
            {
                img: "/profiles/kelter.jpg",
                name: "Jacob Kelter",
                bio: "Jacob is a PhD student in the combined computer science + learning sciences program at Northwestern University. He is broadly interested in using computational tools for exploring and modeling complex systems and for helping people learn about them. Since mid-2020 he has led the development of agent-based models to better understand various aspects of the Xalgorithms oughtomation system and Earth Reserve Assurance framework.",
            }
        ]
    }
}

export default communityContent