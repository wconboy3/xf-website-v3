import style from './Footer.module.css'

export default function Footer() {
    return (
        <>
            <div className={style.footerhold}>
                <div>
                    <p>
                    Text Graphics CC-by International 4.0  
                    <br />
                    Core Software Apache 2.0 
                    <br />
                    Extensions Affero GPL 3.0
                    </p>
                </div>
                <div>
                    <a href="https://xalgo-system.herokuapp.com/" target="_blank" className={style.footerlink}>
                        XRM dev
                    </a>
                </div>
                <div>
                    <a href="https://development.xalgorithms.org/" target="_blank" className={style.footerlink}>
                        Developer Docs
                    </a>
                    <a href="https://gitlab.com/xalgorithms-alliance" target="_blank" className={style.footerlink}>
                        Source Code
                    </a>
                    <a href="https://suspicious-goodall-5c206e.netlify.app/" target="_blnk" className={style.footerlink}>
                        Design and Brand
                    </a>
                </div>
                <div>
                    <a href="https://twitter.com/Xalgorithms" target="_blank" className={style.footerlink}>
                        Twitter
                    </a>
                    <a href="https://www.linkedin.com/company/11028670/admin/" target="_blank" className={style.footerlink}> 
                        LinkedIn
                    </a>
                    <a href="mailto:jpotvin@xalgorithms.org " target="_blank" className={style.footerlink}>
                        Email
                    </a>
                </div>
                <div>
                    <a className={style.footerlink}>
                        Donate
                    </a>
                    <a href="https://wordpress-309334-1044070.cloudwaysapps.com/" target="_blank" className={style.footerlink}>
                        Archive
                    </a>
                </div>
            </div>
        </>
    )
}