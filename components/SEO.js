import Head from 'next/head'
import { useState, useEffect } from 'react'

export default function SEO ({img, title, description, tags=[]}) {
    
    const [tagstring, SetTagstring] = useState("")
    
    const renderTags = () => {
        tags.map(({tag}) => {
            SetTagstring(tagstring => tagstring.concat(tag + ", "))
        }) 
    }

    useEffect(() => {
        renderTags() 
    }, []);

    return (
        <Head>
            <title>Xalgorithms Foundation</title>
            <link rel="icon" href="/favicon.ico" />
            <meta name="description" content={description} />

            {/*Google SEO*/}
            <meta itemprop="name" content={title}/>
            <meta itemprop="description" content={description}/>
            <meta itemprop="image" content={img}/>

            {/*Twitter SEO*/}
            <meta name="twitter:card" content="summary_large_image"/>
            <meta name="twitter:title" content={title}/>
            <meta name="twitter:description" content={description}/>
            <meta name="twitter:image:src" content={img}/>

            {/*Open Graph SEO*/}
            <meta property="og:title" content={title}/>
            <meta property="og:type" content="website"/>
            <meta property="og:url" content=""/>
            <meta property="og:image" content={img}/>
            <meta property="og:image:width" content=""/>
            <meta property="pg:image:height" content=""/>
            <meta property="og:description" content={description}/>
            <meta property="og:site_name" content=""/>
            <meta property="article:tag" content={tagstring}/>

        </Head>
    )
}