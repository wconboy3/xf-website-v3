## Welcome

Welcome to the next iteration of the Xalgorithms Foundation website.

## Contributing Writing

If you would like to contribute a piece of writing or supply an edit, please read the writing guideline in the `/writing` directory.

## Build Pipelines

You can visit the live website [here](https://hopeful-blackwell-2d9bad.netlify.app/).

You can see the `drafts` branch live [here](https://objective-villani-f45a00.netlify.app/).